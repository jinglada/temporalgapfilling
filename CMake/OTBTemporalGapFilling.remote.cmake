#Contact: Jordi Inglada <jordi.inglada@cesbio.eu>

otb_fetch_module(OTBTemporalGapFilling
  "Tools for performing temporal gapfilling for image time series."
  GIT_REPOSITORY http://tully.ups-tlse.fr/jordi/temporalgapfilling.git
  GIT_TAG 402d41f689ac1f618007e7eb961b06284771afd3
  )
