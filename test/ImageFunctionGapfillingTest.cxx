/*=========================================================================

  Program:   gapfilling
  Language:  C++

  Copyright (c) Jordi Inglada. All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "otbTemporalGapfillingTests.h"
#include <fstream>
#include <tuple>
#include "otbImageFileWriter.h"

using ImageType = otb::VectorImage<ValueType, 2>;

using LinearFunctorType =
  GapFilling::LinearGapFillingFunctor<ImageType::PixelType>;

void create_image(std::string ima_name, PixelType pix)
{
  ImageType::IndexType start = {{0,0}};
  ImageType::SizeType size = {{1,1}};
  ImageType::RegionType region;
  region.SetSize(size);
  region.SetIndex(start);

  auto in_image = ImageType::New();
  in_image->SetRegions(region);
  in_image->SetNumberOfComponentsPerPixel(pix.GetSize());
  in_image->Allocate();
  in_image->SetPixel(start, pix);

  auto writer = otb::ImageFileWriter<ImageType>::New();
  writer->SetInput(in_image);
  writer->SetFileName(ima_name);
  writer->Update();
}
std::tuple<std::string, std::string, std::string, std::string, std::string> 
create_data_files(std::string prefix, size_t cpd)
{
 
  unsigned int nbDates = 0;
  unsigned int nbOutputDates = 0;
  auto date_file = prefix+"_dates.txt";
  std::ofstream df(date_file);
  df << "20140101\n"; nbDates++;
  df << "20140106\n"; nbDates++;
  df << "20140112\n"; nbDates++;
  df << "20140115\n"; nbDates++;
  df.close();
  auto out_date_file = prefix+"_out_dates.txt";
  std::ofstream odf(out_date_file);
  odf << "20140101\n"; nbOutputDates++;
  odf << "20140106\n"; nbOutputDates++;
  odf << "20140109\n"; nbOutputDates++;
  odf << "20140112\n"; nbOutputDates++;
  odf << "20140115\n"; nbOutputDates++;
  odf.close();

  unsigned int nbComponents(nbDates*cpd);
  PixelType pix{nbComponents};
  for(size_t i=0; i<nbComponents; i++)
    pix[i] = i;

  for(size_t i=0; i<cpd; i++)
    pix[cpd*2+i] = 20;
  auto in_image_file = prefix+"_in_image.tif";
  create_image(in_image_file,pix);

  auto mask_file = prefix+"_mask.tif";
  pix.Fill(0);
  for(size_t i=0; i<cpd; i++)
    pix[cpd*2+i] = 1;
  create_image(mask_file,pix);

  auto out_image_file = prefix+"_out_image.tif";
  return std::make_tuple(in_image_file, mask_file, out_image_file, date_file, 
                         out_date_file);
}

int imageFunctionGapfillingTest(int argc, char * argv[])
{
  if(argc!=3)
    {
    std::cout << argc-1 << " arguments given" << std::endl;
    std::cout << "Usage: " << argv[0] << " file_path components_per_date" 
              << std::endl;
    return EXIT_FAILURE;
    }

  size_t cpd = std::atoi(argv[2]);
  std::string in_image_file, mask_file, out_image_file, date_file, out_date_file;

  std::stringstream prefix;
  prefix << std::string(argv[1])+"/id" << cpd;
  std::tie(in_image_file, mask_file, out_image_file, date_file, out_date_file) = 
    create_data_files(prefix.str(), cpd);

  using TPixel = typename ImageType::PixelType;
  using TValue = typename TPixel::ValueType;


  TPixel dv, odv;
  using TValue = typename ImageType::PixelType::ValueType;

  {
  auto date_vec = GapFilling::parse_date_file(date_file);
  std::vector<TValue> doy_vector(date_vec.size(), TValue{0});
  std::transform(std::begin(date_vec), std::end(date_vec),
                 std::begin(doy_vector), GapFilling::doy_multi_year());
  dv = TPixel(doy_vector.data(), doy_vector.size());
  }
  {
  auto date_vec = GapFilling::parse_date_file(out_date_file);
  std::vector<TValue> doy_vector(date_vec.size(), TValue{0});
  std::transform(std::begin(date_vec), std::end(date_vec),
                 std::begin(doy_vector), GapFilling::doy_multi_year());
  odv = TPixel(doy_vector.data(), doy_vector.size());
  }

  auto readerIma = otb::ImageFileReader<ImageType>::New();
  auto readerMask = otb::ImageFileReader<ImageType>::New();
  readerIma->SetFileName(in_image_file);
  readerMask->SetFileName(mask_file);

  ImageType::Pointer inIma = readerIma->GetOutput();
  ImageType::Pointer maskIma = readerMask->GetOutput();

  using MultiComponentFunctorType =
    GapFilling::MultiComponentTimeSeriesFunctorAdaptor<typename ImageType::PixelType,
                                                       LinearFunctorType>;
  auto filter = GapFilling::BinaryFunctorImageFilterWithNBands<ImageType, ImageType, 
                                                               ImageType,
                                                               LinearFunctorType>::New();
  auto filter_mc =
    GapFilling::BinaryFunctorImageFilterWithNBands<ImageType, 
                                                   ImageType, 
                                                   ImageType,
                                                   MultiComponentFunctorType>::New();

  GapFilling::gapfill_time_series<ImageType, 
                                  LinearFunctorType, 
                                  MultiComponentFunctorType>(inIma,
                                                             maskIma,
                                                             filter,
                                                             filter_mc,
                                                             cpd,
                                                             dv);

  auto writer = otb::ImageFileWriter<ImageType>::New();
  if(cpd==1)
    {
    filter->UpdateOutputInformation();
    writer->SetInput(filter->GetOutput());
    }
  else
    {
    filter_mc->UpdateOutputInformation();
    writer->SetInput(filter_mc->GetOutput());
    }
  writer->SetFileName(out_image_file);
  writer->Update();

  auto readerIma2 = otb::ImageFileReader<ImageType>::New();
  auto readerMask2 = otb::ImageFileReader<ImageType>::New();
  readerIma2->SetFileName(in_image_file);
  readerMask2->SetFileName(mask_file);

  ImageType::Pointer inIma2 = readerIma2->GetOutput();
  ImageType::Pointer maskIma2 = readerMask2->GetOutput();

  inIma2->UpdateOutputInformation();
  maskIma2->UpdateOutputInformation();

  std::stringstream prefix2;
  prefix2 << std::string(argv[1])+"/od" << cpd;
  std::tie(in_image_file, mask_file, out_image_file, date_file, out_date_file) = 
    create_data_files(prefix2.str(),cpd);

  {
  auto date_vec = GapFilling::parse_date_file(date_file);
  std::vector<TValue> doy_vector(date_vec.size(), TValue{0});
  std::transform(std::begin(date_vec), std::end(date_vec),
                 std::begin(doy_vector), GapFilling::doy_multi_year());
  dv = TPixel(doy_vector.data(), doy_vector.size());
  }
  {
  auto date_vec = GapFilling::parse_date_file(out_date_file);
  std::vector<TValue> doy_vector(date_vec.size(), TValue{0});
  std::transform(std::begin(date_vec), std::end(date_vec),
                 std::begin(doy_vector), GapFilling::doy_multi_year());
  odv = TPixel(doy_vector.data(), doy_vector.size());
  }

  std::cout << "With output dates" << std::endl;
  GapFilling::gapfill_time_series<ImageType, 
                                  LinearFunctorType, 
                                  MultiComponentFunctorType>(inIma2,
                                                             maskIma2,
                                                             filter,
                                                             filter_mc,
                                                             cpd,
                                                             dv,
                                                             odv);
  return EXIT_SUCCESS;
}
