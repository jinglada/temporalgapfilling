# =========================================================================
# Program:   gapfilling
# Language:  C++
#
# Copyright (c) CESBIO. All rights reserved.
#
# See LICENSE for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
otb_module_test()

set(${otb-module}Tests
  otbTemporalGapfillingTests.cxx
  LinearGapfillingTest.cxx
  SplineGapfillingTest.cxx
  MultiComponentTest.cxx
  ImageFunctionGapfillingTest.cxx
  InterlacingTests.cxx
  MultiComponentOutputDatesTest.cxx)

add_executable(otbTemporalGapFillingTests ${${otb-module}Tests})
target_link_libraries(otbTemporalGapFillingTests ${${otb-module}-Test_LIBRARIES}  ${GSL_LIBRARIES})
otb_module_target_label(otbTemporalGapFillingTests)

otb_add_test(NAME linearGapfillingTest
  COMMAND otbTemporalGapFillingTests linearGapfillingTest)

otb_add_test(NAME splineGapfillingTest
  COMMAND otbTemporalGapFillingTests splineGapfillingTest)

otb_add_test(NAME multiIdentityComponentGapfillingTest
  COMMAND otbTemporalGapFillingTests multiComponentGapfillingTest 0)

otb_add_test(NAME multiLinearComponentGapfillingTest
  COMMAND otbTemporalGapFillingTests multiComponentGapfillingTest 1)

otb_add_test(NAME multiSplineComponentGapfillingTest
  COMMAND otbTemporalGapFillingTests multiComponentGapfillingTest 2)

otb_add_test(NAME imageFunctionGapfillingTest
  COMMAND otbTemporalGapFillingTests imageFunctionGapfillingTest ${TEMP}/ 1)

otb_add_test(NAME imageFunctionGapfillingMultiCompTest
  COMMAND otbTemporalGapFillingTests imageFunctionGapfillingTest ${TEMP}/ 2)

otb_add_test(NAME deinterlacingTest
  COMMAND otbTemporalGapFillingTests deinterlacingTest)

otb_add_test(NAME interlacingTest
  COMMAND otbTemporalGapFillingTests interlacingTest)

otb_add_test(NAME linearWithOutputDatesGapfillingTest
  COMMAND otbTemporalGapFillingTests linearWithOutputDatesGapfillingTest)

otb_add_test(NAME multiComponentOutputDatesGapfillingTest
  COMMAND otbTemporalGapFillingTests multiComponentOutputDatesGapfillingTest)

otb_add_test(NAME linearWithRealDatesTest
  COMMAND otbTemporalGapFillingTests linearWithRealDates)

otb_add_test(NAME splineWithRealDatesTest
  COMMAND otbTemporalGapFillingTests splineWithRealDates)

otb_test_application(NAME otbImageTimeSeriesGapFillingTest
  APP  ImageTimeSeriesGapFilling
  OPTIONS 
  -in ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesNDVIRoi.tif
  -mask ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesMasksRoi.tif
  -out ${TEMP}/GapFilledTimeSeriesRoi.tif
  -comp 1
  -it linear
  VALID   --compare-image ${EPSILON_15}
  ${OTBTemporalGapFilling_SOURCE_DIR}/data/GapFilledTimeSeriesRoi.tif
  ${TEMP}/GapFilledTimeSeriesRoi.tif)

otb_test_application(NAME otbImageTimeSeriesGapFillingInputDatesTest
  APP  ImageTimeSeriesGapFilling
  OPTIONS 
  -in ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesNDVIRoi.tif
  -mask ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesMasksRoi.tif
  -out ${TEMP}/GapFilledTimeSeriesRoiInputDates.tif
  -comp 1
  -it linear
  -id ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesInputDateFile.txt
  VALID   --compare-image ${EPSILON_15}
  ${OTBTemporalGapFilling_SOURCE_DIR}/data/GapFilledTimeSeriesRoiInputDates.tif
  ${TEMP}/GapFilledTimeSeriesRoiInputDates.tif)

otb_test_application(NAME otbImageTimeSeriesGapFillingOutputDatesTest
  APP  ImageTimeSeriesGapFilling
  OPTIONS 
  -in ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesNDVIRoi.tif
  -mask ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesMasksRoi.tif
  -out ${TEMP}/GapFilledTimeSeriesRoiOutputDates.tif
  -comp 1
  -it linear
  -id ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesInputDateFile.txt
  -od ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesInputDateFile.txt
  VALID   --compare-image ${EPSILON_15}
  ${OTBTemporalGapFilling_SOURCE_DIR}/data/GapFilledTimeSeriesRoiOutputDates.tif
  ${TEMP}/GapFilledTimeSeriesRoiOutputDates.tif)

otb_test_application(NAME otbImageTimeSeriesGapFillingDiferentInputOutputDatesTest
  APP  ImageTimeSeriesGapFilling
  OPTIONS
  -in ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesNDVIRoi.tif
  -mask ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesMasksRoi.tif
  -out ${TEMP}/GapFilledTimeSeriesRoiDiferentInputOutputDates.tif
  -comp 1
  -it linear
  -id ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesInputDateFile.txt
  -od ${OTBTemporalGapFilling_SOURCE_DIR}/data/TimeSeriesOutputDateFile.txt
  VALID   --compare-image ${EPSILON_15}
  ${OTBTemporalGapFilling_SOURCE_DIR}/data/GapFilledTimeSeriesRoiDiferentInputOutputDates.tif
  ${TEMP}/GapFilledTimeSeriesRoiDiferentInputOutputDates.tif)
