/*=========================================================================

  Program:   gapfilling
  Language:  C++

  Copyright (c) Jordi Inglada. All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbTemporalGapFilling.h"

namespace otb
{
namespace Wrapper
{

class ImageTimeSeriesGapFilling: public Application
{
public:
  typedef ImageTimeSeriesGapFilling Self;
  typedef Application                   Superclass;
  typedef itk::SmartPointer<Self> Pointer; 
  typedef itk::SmartPointer<const Self> ConstPointer;

  itkNewMacro(Self);
  itkTypeMacro(ImageTimeSeriesGapFilling, otb::Application);

  using ImageType = otb::VectorImage<float, 2>;
  using LinearFunctorType =
    GapFilling::LinearGapFillingFunctor<ImageType::PixelType>;
  using SplineFunctorType =
    GapFilling::SplineGapFillingFunctor<ImageType::PixelType>;


  using LinearMultiComponentFunctorType =
    GapFilling::MultiComponentTimeSeriesFunctorAdaptor<
    typename FloatVectorImageType::PixelType,
    LinearFunctorType>;

  using LinearFilterType =  
    GapFilling::BinaryFunctorImageFilterWithNBands<FloatVectorImageType, 
                                                   FloatVectorImageType, 
                                                   FloatVectorImageType,
                                                   LinearFunctorType>;

  using LinearMCFilterType = 
    GapFilling::BinaryFunctorImageFilterWithNBands<FloatVectorImageType, 
                                                   FloatVectorImageType, 
                                                   FloatVectorImageType,
                                                   LinearMultiComponentFunctorType>;

  using SplineMultiComponentFunctorType =
    GapFilling::MultiComponentTimeSeriesFunctorAdaptor<
    typename FloatVectorImageType::PixelType,
    SplineFunctorType>;

  using SplineFilterType =  
    GapFilling::BinaryFunctorImageFilterWithNBands<FloatVectorImageType, 
                                                   FloatVectorImageType, 
                                                   FloatVectorImageType,
                                                   SplineFunctorType>;

  using SplineMCFilterType =  
    GapFilling::BinaryFunctorImageFilterWithNBands<FloatVectorImageType, 
                                                   FloatVectorImageType, 
                                                   FloatVectorImageType,
                                                   SplineMultiComponentFunctorType>;
  
  using TPixel = typename FloatVectorImageType::PixelType;
  using TValue = typename TPixel::ValueType;
private:
  void DoInit() override
  {
    SetName("ImageTimeSeriesGapFilling");
    SetDescription("Time series gapfilling.");

    // Documentation
    SetDocLongDescription("This application performs a temporal gapfilling" 
                          "of an image time series.");
    SetDocLink("http://tully.ups-tlse.fr/jordi/temporalgapfilling#tab-readme");
    SetDocLimitations("None");
    SetDocAuthors("Jordi Inglada");

    AddDocTag(Tags::Filter);
    AddDocTag("MultiTemporal");
    
    AddParameter(ParameterType_InputImage,  "in",   "Input time series");
    SetParameterDescription("in", "Input time series: " 
                            "A multiband image resulting from the concatenation " 
                            "of your different dates. If the images are multispectral, "
                            "the order of the bands should be: "
                            "date1b1 date1b2 ... date1bN date2b1 date2b2 ...");
    MandatoryOn("in");

    AddParameter(ParameterType_InputImage,  "mask",   "Mask time series");
    SetParameterDescription("mask", "A validity mask (you choose what validity means: "
                            "clouds, saturation, etc.; a valid pixel has a value of 0). "
                            "This is a time series with the same order of dates as "
                            "the input time series.");
    MandatoryOn("mask");

    AddParameter(ParameterType_OutputImage, "out",  "Output time series");
    SetParameterDescription("out", "Output time series: The gapfilled time series. "
                            "It will have the same number of dates as the "
                            "input time series unless you provide an "
                            "output date file (see -od).");
    MandatoryOn("out");

    AddParameter(ParameterType_Int, "comp", "Number of components per date.");
    SetParameterDescription("comp", "The number of bands of each date "
                            "(all dates have to have the same number of bands, of course).");
    AddParameter(ParameterType_String, "it", 
                   "Interpolation type (linear, spline)");
    AddParameter(ParameterType_String, "id", 
                 "Input date file");
    SetParameterDescription("id", "An ascii file containing the dates of the input time series. "
                            "The date format is YYYYMMDD. See this example: " 
                            "http://tully.ups-tlse.fr/jordi/temporalgapfilling/blob/master/data/TimeSeriesInputDateFile.txt "
                            "If no date file is provided, the time gap between "
                            "dates is supposed to be regular.");
    MandatoryOff("id");
    AddParameter(ParameterType_String, "od", 
                 "Output date file");
    SetParameterDescription("od", "An ascii file containing the dates of the output time series. "
                            "Same format as the input date file. "
                            "If provided, the output image time series will be "
                            "resampled to correspond to the dates provided in the "
                            "file. This is useful if you want to have a regular temporal " 
                            "sampling when your input time series is irregular. "
                            "If no file is provided, the input dates are used.");
    MandatoryOff("od");

    AddRAMParameter();

    SetDocSeeAlso(" ");

    // Doc example parameter settings
    SetDocExampleParameterValue("in", "TimeSeriesNDVIRoi.tif");
    SetDocExampleParameterValue("mask", "TimeSeriesMasksRoi.tif");
    SetDocExampleParameterValue("out", "GapFilledTimeSeriesRoiOutputDatesµ.tif");
    SetDocExampleParameterValue("comp", "1");
    SetDocExampleParameterValue("it", "linear");
    SetDocExampleParameterValue("id", "TimeSeriesInputDateFile.txt");
    SetDocExampleParameterValue("od", "TimeSeriesInputDateFile.txt");
    
    SetOfficialDocLink();
  }

  void DoUpdateParameters() override
  {
  }

  void DoExecute() override
  {  

    FloatVectorImageType::Pointer inputImage = this->GetParameterImage("in");
    FloatVectorImageType::Pointer maskImage = this->GetParameterImage("mask");

    TPixel dv, odv;

    auto cpd = GetParameterInt("comp");

    PrepareDateVectors(dv, odv);

    if(GetParameterString("it") == "linear")
      {
      m_linear_filter = LinearFilterType::New();
      m_linear_filter_mc = LinearMCFilterType::New();
      Interpolation<LinearFilterType, LinearMCFilterType, 
                    LinearFunctorType, 
                    LinearMultiComponentFunctorType>(inputImage, maskImage, 
                                                     cpd, dv, odv, 
                                                     m_linear_filter,
                                                     m_linear_filter_mc,
                                                     "linear");
      }
    else
      {
      m_spline_filter = SplineFilterType::New();
      m_spline_filter_mc = SplineMCFilterType::New();
      Interpolation<SplineFilterType, SplineMCFilterType, 
                    SplineFunctorType, 
                    SplineMultiComponentFunctorType>(inputImage, maskImage, 
                                                     cpd, dv, odv, 
                                                     m_spline_filter,
                                                     m_spline_filter_mc,
                                                     "spline");
      }
  }

  void PrepareDateVectors(TPixel& dv, TPixel& odv) 
  {
    std::string in_date_file{""};
    std::string out_date_file{""};
    std::tm first_in_date{};
    if(IsParameterEnabled("id") && !IsParameterEnabled("od"))
      {
      in_date_file = GetParameterString("id");
      otbAppLogINFO( "Using date file " << in_date_file );
      
      auto date_vec = GapFilling::parse_date_file(in_date_file);
      first_in_date = date_vec[0];
      std::vector<TValue> doy_vector(date_vec.size(), TValue{0});
      std::transform(std::begin(date_vec), std::end(date_vec),
                     std::begin(doy_vector), GapFilling::doy_multi_year());
      dv = TPixel(doy_vector.data(), doy_vector.size());
      odv = dv;
      }

    else if (IsParameterEnabled("id") && IsParameterEnabled("od"))
      {
      in_date_file = GetParameterString("id");
      otbAppLogINFO( "Using date file " << in_date_file );
      out_date_file = GetParameterString("od");
      otbAppLogINFO( "Using output date file " << out_date_file );

      auto in_date_vec = GapFilling::parse_date_file(in_date_file);
      first_in_date = in_date_vec[0];
      std::vector<TValue> doy_vector_in(in_date_vec.size(), TValue{0});

      auto out_date_vec = GapFilling::parse_date_file(out_date_file);
      auto first_out_date = out_date_vec[0];
      auto d_init = 0;

      if(first_out_date < first_in_date)
        {
        d_init = GapFilling::doy(first_out_date);
        std::transform(std::begin(in_date_vec), std::end(in_date_vec),
                     std::begin(doy_vector_in), GapFilling::doy_multi_year(d_init));
        dv = TPixel(doy_vector_in.data(), doy_vector_in.size());
        }

      else
        {
        std::transform(std::begin(in_date_vec), std::end(in_date_vec),
                     std::begin(doy_vector_in), GapFilling::doy_multi_year());
        dv = TPixel(doy_vector_in.data(), doy_vector_in.size());
        d_init = dv[0];
        }

      std::vector<TValue> doy_vector_out(out_date_vec.size(), TValue{0});
      std::transform(std::begin(out_date_vec), std::end(out_date_vec),
                     std::begin(doy_vector_out), GapFilling::doy_multi_year(d_init));
      odv = TPixel(doy_vector_out.data(), doy_vector_out.size());
      }

  }

  template <typename FilterType, typename MCFilterType, typename FunctorType, 
            typename MCFunctorType>
  void Interpolation(FloatVectorImageType::Pointer inputImage,
                     FloatVectorImageType::Pointer maskImage,
                     int cpd, TPixel dv, TPixel odv,   
                     typename FilterType::Pointer filter,
                     typename MCFilterType::Pointer filter_mc,
                     std::string int_type_str)
  {
    otbAppLogINFO( "Using " << int_type_str << " interpolation and " << cpd
              << " components per date " );
    
    GapFilling::gapfill_time_series<ImageType, 
                                    FunctorType,
                                    MCFunctorType>(
                                      inputImage,
                                      maskImage,
                                      filter,
                                      filter_mc,
                                      cpd,
                                      dv,
                                      odv);
    if(cpd==1)
      SetParameterOutputImage("out", filter->GetOutput());
    else
      SetParameterOutputImage("out", filter_mc->GetOutput());
  }


  LinearFilterType::Pointer m_linear_filter;
  LinearMCFilterType::Pointer m_linear_filter_mc;
  SplineFilterType::Pointer m_spline_filter;
  SplineMCFilterType::Pointer m_spline_filter_mc;
  FloatVectorImageType::Pointer outputImage;

};
} // end namespace Wrapper
} // end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::ImageTimeSeriesGapFilling)
